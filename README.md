# Challenge Bruno

**Objective:** Analysis of impact of Hurricane names in US names between 5 years before and after the hurricane occurrence.

## Data Understanding

BigQuery public datasets:

* _bigquery-public-data.noaa_hurricanes.hurricanes_:     
    * **Description:** Hurricane occurrences      
    * **Usable columns:** name, season      
    * **Observations:** NOT_NAMED hurricanes, names with non-letters characters, reports more than once     
* _bigquery-public-data.usa_names.usa_1910_current_:    
    * **Description:** Names occurrences in US     
    * **Usable columns:** name, year, number     
    * **Observations:** names more than once a year, range 1910-current     


## Proposed Solution

Compare the average of occurrence of a hurricane�s name in the US Names dataset before and after it happened. For example:

Hurricane: Bruno  
Year: 2018

Babies names occurrence of Bruno between 2013-2017:  
2013: 75      
2014: 20  
2015: 30  
2016: 40  
2017: 60  
Avg.: (75+20+30+40+60)/5= 45

Babies names occurrence of Bruno between 2019-2023:  
2019: 75  
2020: 20  
2021: 10  
2022: 40  
2023: 30  
Avg.: (75+20+10+40+30)/5= 35

The occurrence of Bruno after the Hurricane decreased 23%, comparing 5 years after and before. That is for one hurricane, if we do for all of them we can get a better clue.

## Decisions

* **Keep it simple as possible.**
* Hurricanes data treatment was made by filtering names with non-letter characters and names = NOT_NAMED.
* Considered only the first time of Hurricane reported.
* Considered the maximum number of occurrence in US Names reported in a year.
* The average comparison between periods is a good estimate and avoid the use of complex statistical methods.
* Environment was chosen by experience time: Java, Dataflow, Firestore (in Datastore mode)
* Dataflow does't have a built-in connector for Firestore yet so I used the DatastoreIO to write on Firestore in Datastore mode.
* Analysis were made in Data studio reading data from BigQuery. The link to this report is on **Analysis and Visualizations** section.

## Getting Started

### Prerequisites
Maven, JDK 1.8, GCP project set up (BigQuery dataset, Datastore, Dataflow, Permissions, APIs enabled)

### Building
```
mvn install
```

### Running tests
```
mvn test
```
### Execution

Deploy self executing jar to Dataflow

```bash
java -jar target/qlouder-hurricane-dataflow-1.0.0.0-SNAPSHOT.jar --runner=DataflowRunner /
--project=challenge-bruno --dataset=BRUNO --table=HURRICANE_AVERAGES /
--stagingLocation=gs://challenge-bruno/stage /
--tempLocation=gs://challenge-bruno/temp --jobName=challenge-bruno-dataflow
```

## Analysis and Visualizations

[Data Studio report](https://datastudio.google.com/open/1uTrQnfLZjp9BRpGEXoF49e1fRxq9Uhmd)

## Conclusion

Based on visualizations in the report above, we can see that there is no significative correlation between the Hurricane names and baby names. In the time series on page 1 we see how the sum of averages before and after are proximately close each other. Also, the pizza graph on page 3 shows us almost same increase and decrease in names. Also in page 3, we can see how the distributions of averages are close. Although this pushes us to one direction, we can not take deep conclusions on this exercise. Data was treated poorly and averages sometimes were compared aggregated each other instead of separated.

## Built With

* [Maven](https://maven.apache.org/) - Dependency Management

## Authors

* **Bruno Liberal** - [Github](https://github.com/brunoliberal) [Bitbucket](https://bitbucket.org/brunoliberal/)

## License
[MIT](https://choosealicense.com/licenses/mit/)