package com.qlouder.dataflow.hurricane;

import java.io.IOException;

import org.apache.beam.sdk.io.gcp.bigquery.BigQueryOptions;
import org.apache.beam.sdk.options.PipelineOptionsFactory;
import org.apache.beam.sdk.testing.TestPipeline;
import org.apache.beam.sdk.testing.TestPipelineOptions;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class HurricaneNamesPipelineIT {

  public interface HurricaneNamesOptionsITOptions
      extends HurricaneNamesOptions, TestPipelineOptions, BigQueryOptions {}

  @BeforeClass
  public static void setUp() {
    PipelineOptionsFactory.register(HurricaneNamesOptionsITOptions.class);
  }

  @Test
  @Ignore
  public void testE2EHurricaneNamesPipeline() throws IOException {
    HurricaneNamesOptionsITOptions options = TestPipeline.testingPipelineOptions().as(HurricaneNamesOptionsITOptions.class);
    options.setDataset("BRUNO_IT");
    options.setTempLocation("gs://challenge-bruno/temp");
    //options.setOnSuccessMatcher(new BigqueryMatcher(options.getAppName(), options.getProject(), "", DEFAULT_OUTPUT_CHECKSUM)));
    HurricaneNamesPipeline.runPipeline(options);
  }
  
  @AfterClass
  public static void cleanUp() {
/*    final BigQuery bq = com.google.cloud.bigquery.BigQueryOptions.getDefaultInstance().getService();
    bq.query(QueryRequest.newBuilder("DELETE FROM BRUNO_IT.HURRICANE_AVERAGES WHERE TRUE")
        .setUseLegacySql(false).build());*/
  }
}
