package com.qlouder.dataflow.hurricane.function;

import static com.qlouder.dataflow.hurricane.Constants.DS_AVG;
import static com.qlouder.dataflow.hurricane.Constants.DS_KIND;
import static com.qlouder.dataflow.hurricane.Constants.DS_YEAR;
import static com.qlouder.dataflow.hurricane.Constants.DS_AVG_TYPE;
import static com.qlouder.dataflow.hurricane.Constants.DS_NAME;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.beam.sdk.testing.PAssert;
import org.apache.beam.sdk.testing.TestPipeline;
import org.apache.beam.sdk.transforms.Create;
import org.apache.beam.sdk.transforms.ParDo;
import org.apache.beam.sdk.transforms.View;
import org.apache.beam.sdk.values.KV;
import org.apache.beam.sdk.values.PCollection;
import org.apache.beam.sdk.values.PCollectionView;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import com.google.api.services.bigquery.model.TableRow;
import com.google.datastore.v1.Entity;
import com.google.datastore.v1.Value;
import com.google.datastore.v1.client.DatastoreHelper;
import com.qlouder.dataflow.hurricane.Constants;
import com.qlouder.dataflow.hurricane.function.ParseHurricaneRowsFn;
import com.qlouder.dataflow.hurricane.function.ParseNamesRowsFn;
import com.qlouder.dataflow.hurricane.function.GenerateEntities;

@RunWith(JUnit4.class)
public class TransformsTest {

  @Rule
  public TestPipeline p = TestPipeline.create();
  
  @Test
  public void testParseHurricaneeRows() {
    List<TableRow> rows = new ArrayList<>();
    addRow(rows, "John", "1940");
    addRow(rows, "MARY", "1948");
    addRow(rows, "Jim:Cley", "1955");

    PCollection<KV<String, Integer>> result = p.apply(Create.of(rows)).apply(ParDo.of(new ParseHurricaneRowsFn()));
    
    PAssert.that(result).containsInAnyOrder(KV.of("john", 1940), KV.of("mary", 1948));
    
    p.run().waitUntilFinish();
  }
  
  @Test
  public void testParseNamesRows() {
    PCollectionView<Map<String, Integer>> hurricaneView = p
        .apply("create view", Create.of(KV.of("john", 1940), KV.of("mary", 1948))).apply(View.asMap());
    
    List<TableRow> rows = new ArrayList<>();
    addRow(rows, "John", "1939", "123");
    addRow(rows, "Mary", "1949", "142");
    addRow(rows, "Jim", "1955", "142");
    
    PCollection<KV<String, Integer>> result = p.apply(Create.of(rows))
        .apply(ParDo.of(new ParseNamesRowsFn(hurricaneView)).withSideInputs(hurricaneView));
    
    PAssert.that(result).containsInAnyOrder(KV.of("john$BEFORE$1940", 123), KV.of("mary$AFTER$1948", 142));
    
    p.run().waitUntilFinish();
  }
  
  @Test
  public void testGenerateEntities() {
    PCollection<Entity> result = p.apply(Create.of(KV.of("john$BEFORE$1940", 22.4)))
        .apply(ParDo.of(new GenerateEntities()));

    PAssert.that(result).containsInAnyOrder(getEntityJohn());

    p.run().waitUntilFinish();
  }
  
  @Test
  public void testGenerateTableRows() {
    
    PCollection<TableRow> result = p.apply(Create.of(getEntityJohn()))
        .apply(ParDo.of(new GenerateTableRows()));
    
    TableRow row = new TableRow();
    row.put(Constants.BQ_OUT_NAME, "john");
    row.put(Constants.BQ_OUT_YEAR, "1940");
    row.put(Constants.BQ_OUT_AVG, 22.4);
    row.put(Constants.BQ_OUT_AVG_TYPE, "before");
    
    PAssert.that(result).containsInAnyOrder(row);
    
    p.run().waitUntilFinish();
  }

  private Entity getEntityJohn() {
    Entity.Builder builder = Entity.newBuilder();
    builder.setKey(DatastoreHelper.makeKey(DS_KIND, "john$BEFORE$1940").build());
    builder.putProperties(DS_NAME, Value.newBuilder().setStringValue("john").build());
    builder.putProperties(DS_YEAR, Value.newBuilder().setStringValue("1940").build());
    builder.putProperties(DS_AVG, Value.newBuilder().setDoubleValue(22.4).build());
    builder.putProperties(DS_AVG_TYPE, Value.newBuilder().setStringValue("before").build());
    return builder.build();
  }
  
  private void addRow(List<TableRow> rows, String name, String year) {
    addRow(rows, name, year, null);
  }
 
  private void addRow(List<TableRow> rows, String name, String year, String number) {
    TableRow row = new TableRow();
    row.put(Constants.BQ_NAME, name);
    row.put(Constants.BQ_YEAR, year);
    if (number != null) {
      row.put(Constants.BQ_NUMBER, number);
    }
    rows.add(row);
  }
}
