package com.qlouder.dataflow.hurricane.function;

import static com.qlouder.dataflow.hurricane.Constants.BQ_OUT_AVG;
import static com.qlouder.dataflow.hurricane.Constants.BQ_OUT_AVG_TYPE;
import static com.qlouder.dataflow.hurricane.Constants.BQ_OUT_NAME;
import static com.qlouder.dataflow.hurricane.Constants.BQ_OUT_YEAR;
import static com.qlouder.dataflow.hurricane.Constants.DS_AVG;
import static com.qlouder.dataflow.hurricane.Constants.DS_AVG_TYPE;
import static com.qlouder.dataflow.hurricane.Constants.DS_NAME;
import static com.qlouder.dataflow.hurricane.Constants.DS_YEAR;

import java.util.Map;

import org.apache.beam.sdk.transforms.DoFn;

import com.google.api.services.bigquery.model.TableRow;
import com.google.datastore.v1.Entity;
import com.google.datastore.v1.Value;

public class GenerateTableRows extends DoFn<Entity, TableRow> {

  private static final long serialVersionUID = 1792735256831606349L;

  @ProcessElement
  public void process(ProcessContext c) {
    Entity entity = c.element();
    Map<String, Value> prop = entity.getPropertiesMap();
    
    TableRow row = new TableRow();
    row.set(BQ_OUT_NAME, prop.get(DS_NAME).getStringValue());
    row.set(BQ_OUT_YEAR, prop.get(DS_YEAR).getStringValue());
    row.set(BQ_OUT_AVG, prop.get(DS_AVG).getDoubleValue());
    row.set(BQ_OUT_AVG_TYPE, prop.get(DS_AVG_TYPE).getStringValue());
    
    c.output(row);
  }
}
