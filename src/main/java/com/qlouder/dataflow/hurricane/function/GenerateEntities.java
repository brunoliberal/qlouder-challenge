package com.qlouder.dataflow.hurricane.function;

import org.apache.beam.sdk.transforms.DoFn;
import org.apache.beam.sdk.values.KV;

import com.google.datastore.v1.Entity;
import com.google.datastore.v1.Value;
import com.google.datastore.v1.client.DatastoreHelper;
import static com.qlouder.dataflow.hurricane.Constants.*;

public class GenerateEntities extends DoFn<KV<String, Double>, Entity> {

  private static final long serialVersionUID = 3923697810753739570L;

  @ProcessElement
  public void process(ProcessContext c) {
    KV<String, Double> kv = c.element();
    String nameYear = kv.getKey();
    Double avg = kv.getValue();
    
    if (avg > 0) {
      String[] nameYearSplit;
      String avgType;
      if(nameYear.contains("BEFORE")) {
        nameYearSplit = nameYear.split("\\$BEFORE\\$");
        avgType = "before";
      } else {
        nameYearSplit = nameYear.split("\\$AFTER\\$");
        avgType = "after";
      }
      c.output(createEntity(nameYear, avg, nameYearSplit, avgType));
    }
  }

  private Entity createEntity(String nameYear, Double avg, String[] nameYearSplit, String avgType) {
    Entity.Builder entityBuilder = Entity.newBuilder();
    entityBuilder.setKey(DatastoreHelper.makeKey(DS_KIND, nameYear).build());
    entityBuilder.putProperties(DS_NAME, Value.newBuilder().setStringValue(nameYearSplit[0]).build());
    entityBuilder.putProperties(DS_YEAR, Value.newBuilder().setStringValue(nameYearSplit[1]).build());
    entityBuilder.putProperties(DS_AVG, Value.newBuilder().setDoubleValue(avg).build());
    entityBuilder.putProperties(DS_AVG_TYPE, Value.newBuilder().setStringValue(avgType).build());
    return entityBuilder.build();
  }
}
