package com.qlouder.dataflow.hurricane.function;

import static com.qlouder.dataflow.hurricane.Constants.BQ_NAME;
import static com.qlouder.dataflow.hurricane.Constants.BQ_YEAR;

import org.apache.beam.sdk.transforms.DoFn;
import org.apache.beam.sdk.values.KV;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.api.services.bigquery.model.TableRow;

public class ParseHurricaneRowsFn extends DoFn<TableRow, KV<String, Integer>> {
  
  private static final long serialVersionUID = 82496307138182559L;
  private static final Logger log = LoggerFactory.getLogger(ParseHurricaneRowsFn.class);

  @ProcessElement
  public void processElement(ProcessContext c) {
    TableRow row = c.element();
    try {
      String name = ((String) row.get(BQ_NAME)).toLowerCase();
      Integer year = Integer.valueOf((String) row.get(BQ_YEAR));
      if (!name.matches(".*[^A-Za-z].*")) { // filter names that doesn't contain only letters
        c.output(KV.of(name, year));
      }
    } catch (ClassCastException|NumberFormatException e) {
      log.warn("Failed to parse Hurricane table row" + row.toString(), e);
    }
  }
}
