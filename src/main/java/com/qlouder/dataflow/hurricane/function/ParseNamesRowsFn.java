package com.qlouder.dataflow.hurricane.function;

import static com.qlouder.dataflow.hurricane.Constants.BQ_NAME;
import static com.qlouder.dataflow.hurricane.Constants.BQ_NUMBER;
import static com.qlouder.dataflow.hurricane.Constants.BQ_YEAR;

import java.util.Map;

import org.apache.beam.sdk.transforms.DoFn;
import org.apache.beam.sdk.values.KV;
import org.apache.beam.sdk.values.PCollectionView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.api.services.bigquery.model.TableRow;

public class ParseNamesRowsFn extends DoFn<TableRow, KV<String, Integer>> {

  private static final long serialVersionUID = 286941579185414770L;
  private static final Logger log = LoggerFactory.getLogger(ParseNamesRowsFn.class);
  private PCollectionView<Map<String, Integer>> hurricaneView;

  public ParseNamesRowsFn(PCollectionView<Map<String, Integer>> hurricaneView) {
    this.hurricaneView = hurricaneView;
  }

  @ProcessElement
  public void processElement(ProcessContext c) {
    Map<String, Integer> hurricane = c.sideInput(hurricaneView);
    TableRow row = c.element();
    try {
      String name = ((String) row.get(BQ_NAME)).toLowerCase();
      Integer year = Integer.valueOf((String) row.get(BQ_YEAR));
      Integer number = Integer.valueOf((String) row.get(BQ_NUMBER));
  
      Integer hurYear = hurricane.get(name);
      
      if (hurYear != null) { // if name was a Hurricane name and between 5 years after and before interval
        if (hurYear - 5 <= year && year < hurYear) {
          c.output(KV.of(name + "$BEFORE$" + hurYear, number));
        } else if(hurYear < year && year <= hurYear + 5) {
          c.output(KV.of(name + "$AFTER$" + hurYear, number));
        }   
      }
    } catch (ClassCastException|NumberFormatException e) {
      log.warn("Failed to parse Names table row" + row.toString(), e);
    }
  }
}
