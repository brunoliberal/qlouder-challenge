package com.qlouder.dataflow.hurricane;

public class Constants {
  // BigQuery Input
  public static final String BQ_NAME = "name";
  public static final String BQ_YEAR = "year";
  public static final String BQ_NUMBER = "number";
  
  // Datastore
  public static final String DS_KIND = "HurricaneOccurrence";
  public static final String DS_NAME = "name";
  public static final String DS_YEAR = "year";
  public static final String DS_AVG = "avg";
  public static final String DS_AVG_TYPE = "avg_type";
  
  // BigQuery Output
  public static final String BQ_OUT_NAME = "NAME";
  public static final String BQ_OUT_YEAR = "YEAR";
  public static final String BQ_OUT_AVG = "AVG";
  public static final String BQ_OUT_AVG_TYPE = "TYPE";
  
  private Constants() {}
}
