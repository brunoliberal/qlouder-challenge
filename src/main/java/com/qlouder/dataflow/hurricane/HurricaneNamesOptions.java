package com.qlouder.dataflow.hurricane;

import org.apache.beam.sdk.options.Default;
import org.apache.beam.sdk.options.Description;
import org.apache.beam.sdk.options.PipelineOptions;

public interface HurricaneNamesOptions extends PipelineOptions {

  @Description("GCP Project Id")
  @Default.String("challenge-bruno")
  String getProjectId();
  void setProjectId(String value);
  
  @Description("BigQuery Dataset for Analytics")
  @Default.String("BRUNO")
  String getDataset();
  void setDataset(String value);
  
  @Description("BigQuery Tablename for Analytics")
  @Default.String("HURRICANE_AVERAGES")
  String getTable();
  void setTable(String value);
}
