package com.qlouder.dataflow.hurricane;

import java.io.IOException;
import java.util.Map;

import org.apache.beam.sdk.Pipeline;
import org.apache.beam.sdk.PipelineResult;
import org.apache.beam.sdk.io.gcp.bigquery.BigQueryIO;
import org.apache.beam.sdk.io.gcp.datastore.DatastoreIO;
import org.apache.beam.sdk.options.PipelineOptionsFactory;
import org.apache.beam.sdk.transforms.Mean;
import org.apache.beam.sdk.transforms.Min;
import org.apache.beam.sdk.transforms.ParDo;
import org.apache.beam.sdk.transforms.View;
import org.apache.beam.sdk.values.KV;
import org.apache.beam.sdk.values.PCollection;
import org.apache.beam.sdk.values.PCollectionView;

import com.google.api.services.bigquery.model.TableRow;
import com.google.datastore.v1.Entity;
import com.qlouder.dataflow.hurricane.function.ParseHurricaneRowsFn;
import com.qlouder.dataflow.hurricane.function.ParseNamesRowsFn;
import com.qlouder.dataflow.hurricane.function.GenerateEntities;
import com.qlouder.dataflow.hurricane.function.GenerateTableRows;


/**
 * A pipeline that reads public datasets of Hurricanes and US names occurrences
 * and calculate the average of names occurrence 5 years before and 5 year after
 * the corresponding Hurricane happened.
 * Additional comments were added to the code for better understanding.  
 */
public class HurricaneNamesPipeline {

  // Transforms
  private static final String READ_BQ_HURRICANES = "Read BQ Hurricane";
  private static final String PARSE_HURRICANE_ROWS = "Parse Hurricane rows";
  private static final String READ_BQ_NAMES = "Read BQ US Names";
  private static final String PARSE_NAME_ROWS = "Parse Name rows";
  private static final String CALCULATE_MEAN = "Calculate Mean";
  private static final String GENERATE_ENTITIES = "Generate Entities";
  private static final String WRITE_TO_DATASTORE = "Write to Datastore";
  private static final String WRITE_TO_BIGQUERY = "Write to BigQuery";

  public static void main(String[] args) throws IOException {
    HurricaneNamesOptions options = PipelineOptionsFactory.fromArgs(args).withValidation()
        .as(HurricaneNamesOptions.class);
    runPipeline(options);
  }

  static void runPipeline(HurricaneNamesOptions options) throws IOException {
    Pipeline pipeline = Pipeline.create(options);

    PCollection<TableRow> hurricaneRows = pipeline.apply(READ_BQ_HURRICANES,
        BigQueryIO.readTableRows().fromQuery(getHurricaneQuery()).usingStandardSql());

    // Parse hurricanes rows (Filter names)
    // Get the minimum because it was the first time reported
    PCollectionView<Map<String, Integer>> hurricaneView = hurricaneRows.apply(PARSE_HURRICANE_ROWS,
        ParDo.of(new ParseHurricaneRowsFn())).apply(Min.integersPerKey()).apply(View.asMap());
    
    PCollection<TableRow> namesRows = pipeline.apply(READ_BQ_NAMES,
        BigQueryIO.readTableRows().fromQuery(getBabyNamesQuery()).usingStandardSql());    
    
    // Parse names rows, filter, assign groups
    PCollection<KV<String, Integer>> namesGroups = namesRows.apply(PARSE_NAME_ROWS,
        ParDo.of(new ParseNamesRowsFn(hurricaneView)).withSideInputs(hurricaneView));
    
    PCollection<KV<String, Double>> namesMean = namesGroups.apply(CALCULATE_MEAN, Mean.perKey());
    
    // Parse to Datastore entities (FireStore in Datastore mode)
    PCollection<Entity> entities = namesMean.apply(GENERATE_ENTITIES, ParDo.of(new GenerateEntities()));

    entities.apply(WRITE_TO_DATASTORE, DatastoreIO.v1().write().withProjectId(options.getProjectId()));
    // Save data to BQ only for Analysis
    entities.apply(WRITE_TO_BIGQUERY, ParDo.of(new GenerateTableRows()))
        .apply(BigQueryIO.writeTableRows()
        .to(options.getProjectId() + ":" + options.getDataset() + "." + options.getTable())
        .withWriteDisposition(BigQueryIO.Write.WriteDisposition.WRITE_TRUNCATE)
        .withCreateDisposition(BigQueryIO.Write.CreateDisposition.CREATE_IF_NEEDED));
    
    PipelineResult result = pipeline.run();
    try {
      result.waitUntilFinish();
    } catch (Exception exc) {
      result.cancel();
    }
  }

  private static String getHurricaneQuery() {
    return "SELECT name, season year "
        + "FROM `bigquery-public-data.noaa_hurricanes.hurricanes` "
        + "WHERE name != 'NOT_NAMED' "
        + "AND CAST(season AS INT64) BETWEEN 1915 AND 2013 " // 5 years delta to comparison
        + "GROUP BY name, season";
  }
  
  private static String getBabyNamesQuery() {
    return "SELECT name, year, MAX(number) number "
        + "FROM `bigquery-public-data.usa_names.usa_1910_current` "
        + "GROUP BY name, year";
  }
}